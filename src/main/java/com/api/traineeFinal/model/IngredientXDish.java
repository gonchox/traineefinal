package com.api.traineeFinal.model;

import lombok.Data;

@Data
public class IngredientXDish {
    private int id;
    private Ingredient ingredient;
    private Dish dish;
    private int quantity;
}
