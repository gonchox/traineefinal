package com.api.traineeFinal.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

@Data
public class OrderDetail {
    private int id;
    @JsonBackReference
    private PurchaseOrder purchaseOrder;
    private Dish dish;
}
