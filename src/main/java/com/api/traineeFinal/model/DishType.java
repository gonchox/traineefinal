package com.api.traineeFinal.model;

import com.api.traineeFinal.dto.DishTypeDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DishType {
    private int id;
    private String name;

    public DishType(DishTypeDTO dishTypeDTO){
        this.name = dishTypeDTO.getName();
    }
}
