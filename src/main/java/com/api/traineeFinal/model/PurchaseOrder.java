package com.api.traineeFinal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import java.util.List;

@Data
public class PurchaseOrder {
    private int id;
    @JsonIgnore
    private Dish dish;
    private double totalPrice;
    @JsonManagedReference
    private List<OrderDetail> orderDetails;
}
