package com.api.traineeFinal.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Dish {
    private int id;
    private DishType dishType;
    private String name;
    private String description;
    private double dishPrice;
    private List<IngredientXDish> ingredientXDishes;

    public Dish(int id, DishType dishType, String name, String description, double dishPrice) {
        this.id = id;
        this.dishType = dishType;
        this.name = name;
        this.description = description;
        this.dishPrice = dishPrice;
    }
}
