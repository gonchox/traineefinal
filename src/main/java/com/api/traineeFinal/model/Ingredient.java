package com.api.traineeFinal.model;

import com.api.traineeFinal.dto.IngredientDTO;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Ingredient {
    private int id;
    private String name;
    private double ingredientPrice;

    public Ingredient(IngredientDTO ingredientDTO){
        this.name = ingredientDTO.getName();
        this.ingredientPrice = ingredientDTO.getIngredientPrice();
    }

}
