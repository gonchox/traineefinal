package com.api.traineeFinal.service;

import com.api.traineeFinal.dto.DishTypeDTO;
import com.api.traineeFinal.model.DishType;

import java.util.List;

public interface DishTypeService {
    public List<DishType> getDishTypes();
    public DishType createDishType(DishTypeDTO dishTypeDTO);
    public DishType getDishTypeById(int id);
}
