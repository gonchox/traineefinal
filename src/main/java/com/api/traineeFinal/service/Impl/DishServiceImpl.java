package com.api.traineeFinal.service.Impl;

import com.api.traineeFinal.dto.DishDTO;
import com.api.traineeFinal.dto.IngredientXDishDTO;
import com.api.traineeFinal.model.Dish;
import com.api.traineeFinal.model.DishType;
import com.api.traineeFinal.model.Ingredient;
import com.api.traineeFinal.model.IngredientXDish;
import com.api.traineeFinal.repository.DishRepository;
import com.api.traineeFinal.repository.DishTypeRepository;
import com.api.traineeFinal.repository.IngredientRepository;
import com.api.traineeFinal.repository.IngredientXDishRepository;
import com.api.traineeFinal.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DishServiceImpl implements DishService {
    @Autowired
    private DishRepository dishRepository;

    @Autowired
    private DishTypeRepository dishTypeRepository;

    @Autowired
    private IngredientXDishRepository ingredientXDishRepository;

    @Autowired
    private IngredientRepository ingredientRepository;

    @Override
    public List<Dish> getDishes() {
        List<Dish> dishList = dishRepository.getDishes();
        for (Dish dish : dishList) {
            List<IngredientXDish> ingredientXDishes = ingredientXDishRepository.getIngredientXDishByDishId(dish.getId());
            dish.setIngredientXDishes(ingredientXDishes);
        }
        return dishList;
    }

    @Override
    public Dish createDish(DishDTO dishDTO) {
        DishType dishType = dishTypeRepository.getDishTypeById(dishDTO.getIdDishType());

        Dish dish = new Dish();
        dish.setDishType(dishType);
        dish.setName(dishDTO.getName());
        dish.setDescription(dishDTO.getDescription());

        double dishPrice = 0;
        List<IngredientXDish> ingredientXDishList = new ArrayList<>();
        for (IngredientXDishDTO ingredientXDishDTO : dishDTO.getIngredientXDishDTOS()) {
            Ingredient ingredient = ingredientRepository.getIngredientById(ingredientXDishDTO.getIdIngredient());
            dishPrice += ingredientXDishDTO.getQuantity()* ingredient.getIngredientPrice();
            IngredientXDish ingredientXDish = new IngredientXDish();
            ingredientXDish.setIngredient(ingredient);
            ingredientXDish.setDish(dish);
            ingredientXDish.setQuantity(ingredientXDishDTO.getQuantity());
            ingredientXDishList.add(ingredientXDish);
        }
        dish.setDishPrice(dishPrice);
        dish = dishRepository.createDish(dish);

        for(IngredientXDish ingredientXDish : ingredientXDishList){
            ingredientXDishRepository.createIngredientXDish(ingredientXDish);
        }

        return dish;
    }

    @Override
    public Dish getDishById(int id) {
        Dish dish = dishRepository.getDishById(id);
        List<IngredientXDish> ingredientXDishes = ingredientXDishRepository.getIngredientXDishByDishId(id);
        for (IngredientXDish ingredientXDish : ingredientXDishes) {
            Ingredient ingredient = ingredientRepository.getIngredientById(ingredientXDish.getIngredient().getId());
            ingredientXDish.setIngredient(ingredient);
        }
        dish.setIngredientXDishes(ingredientXDishes);
        return dish;
    }
}
