package com.api.traineeFinal.service.Impl;

import com.api.traineeFinal.dto.OrderDetailDTO;
import com.api.traineeFinal.dto.PurchaseOrderDTO;
import com.api.traineeFinal.model.Dish;
import com.api.traineeFinal.model.OrderDetail;
import com.api.traineeFinal.model.PurchaseOrder;
import com.api.traineeFinal.repository.DishRepository;
import com.api.traineeFinal.repository.OrderDetailRepository;
import com.api.traineeFinal.repository.PurchaseOrderRepository;
import com.api.traineeFinal.service.PurchaseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    private DishRepository dishRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Override
    public List<PurchaseOrder> getPurchaseOrders() {
        List<PurchaseOrder> purchaseOrderList = purchaseOrderRepository.getPurchaseOrders();
        for (PurchaseOrder purchaseOrder : purchaseOrderList) {
            List<OrderDetail> orderDetailList = orderDetailRepository.getOrderDetailByPurchaseOrderId(purchaseOrder.getId());
            purchaseOrder.setOrderDetails(orderDetailList);
        }
        return purchaseOrderList;
    }

    @Override
    public PurchaseOrder createPurchaseOrder(PurchaseOrderDTO purchaseOrderDTO) {
        PurchaseOrder purchaseOrder = new PurchaseOrder();

        double totalPrice = 0;
        List<OrderDetail> orderDetails = new ArrayList<>();
        for (OrderDetailDTO orderDetailDTO : purchaseOrderDTO.getOrderDetails()) {
            Dish dish1 = dishRepository.getDishById(orderDetailDTO.getIdDish());
            totalPrice += dish1.getDishPrice();
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setDish(dish1);
            orderDetails.add(orderDetail);
        }
        purchaseOrder.setTotalPrice(totalPrice);
        purchaseOrder.setOrderDetails(orderDetails);

        // Save the purchase order
        purchaseOrder = purchaseOrderRepository.createPurchaseOrder(purchaseOrder);

        // Save the order details
        for (OrderDetail orderDetail : purchaseOrder.getOrderDetails()) {
            orderDetail.setPurchaseOrder(purchaseOrder);
            orderDetailRepository.createOrderDetail(orderDetail);
        }

        return purchaseOrder;
    }


    @Override
    public PurchaseOrder getPurchaseOrderById(int id) {
        PurchaseOrder purchaseOrder = purchaseOrderRepository.getPurchaseOrderById(id);
        List<OrderDetail> orderDetailList = orderDetailRepository.getOrderDetailByPurchaseOrderId(id);
        for (OrderDetail orderDetail : orderDetailList) {
            Dish dish = dishRepository.getDishById(orderDetail.getDish().getId());
            orderDetail.setDish(dish);
        }
        purchaseOrder.setOrderDetails(orderDetailList);
        return purchaseOrder;
    }
}
