package com.api.traineeFinal.service.Impl;

import com.api.traineeFinal.dto.DishTypeDTO;
import com.api.traineeFinal.model.DishType;
import com.api.traineeFinal.repository.DishTypeRepository;
import com.api.traineeFinal.service.DishTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DishTypeServiceImpl implements DishTypeService {

    @Autowired
    private DishTypeRepository dishTypeRepository;

    @Override
    public List<DishType> getDishTypes() {
        return dishTypeRepository.getDishTypes();
    }

    @Override
    public DishType createDishType(DishTypeDTO dishTypeDTO) {
        DishType dishType = new DishType(dishTypeDTO);
        return dishTypeRepository.createDishType(dishType);
    }

    @Override
    public DishType getDishTypeById(int id) {
        return dishTypeRepository.getDishTypeById(id);
    }
}
