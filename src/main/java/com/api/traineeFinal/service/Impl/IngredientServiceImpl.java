package com.api.traineeFinal.service.Impl;

import com.api.traineeFinal.dto.IngredientDTO;
import com.api.traineeFinal.model.DishType;
import com.api.traineeFinal.model.Ingredient;
import com.api.traineeFinal.repository.IngredientRepository;
import com.api.traineeFinal.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientServiceImpl implements IngredientService {
    @Autowired
    private IngredientRepository ingredientRepository;

    @Override
    public List<Ingredient> getIngredients() {
        return ingredientRepository.getIngredients();
    }

    @Override
    public Ingredient createIngredient(IngredientDTO ingredientDTO) {
        Ingredient ingredient = new Ingredient(ingredientDTO);
        return ingredientRepository.createIngredient(ingredient);
    }

    @Override
    public Ingredient getIngredientById(int id) {
        return ingredientRepository.getIngredientById(id);
    }
}
