package com.api.traineeFinal.service;

import com.api.traineeFinal.dto.DishDTO;
import com.api.traineeFinal.model.Dish;

import java.util.List;

public interface DishService {
    public List<Dish> getDishes();
    public Dish createDish(DishDTO dishDTO);
    public Dish getDishById(int id);
}
