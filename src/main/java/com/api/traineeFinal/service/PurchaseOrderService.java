package com.api.traineeFinal.service;

import com.api.traineeFinal.dto.PurchaseOrderDTO;
import com.api.traineeFinal.model.PurchaseOrder;

import java.util.List;

public interface PurchaseOrderService {

    public List<PurchaseOrder> getPurchaseOrders();
    public PurchaseOrder createPurchaseOrder(PurchaseOrderDTO purchaseOrderDTO);
    public PurchaseOrder getPurchaseOrderById(int id);
}
