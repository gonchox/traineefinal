package com.api.traineeFinal.service;

import com.api.traineeFinal.dto.DishTypeDTO;
import com.api.traineeFinal.dto.IngredientDTO;
import com.api.traineeFinal.model.DishType;
import com.api.traineeFinal.model.Ingredient;

import java.util.List;

public interface IngredientService {
    public List<Ingredient> getIngredients();
    public Ingredient createIngredient(IngredientDTO ingredientDTO);
    public Ingredient getIngredientById(int id);
}
