package com.api.traineeFinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TraineeFinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(TraineeFinalApplication.class, args);
	}

}
