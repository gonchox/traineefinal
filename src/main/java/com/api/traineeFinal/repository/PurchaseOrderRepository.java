package com.api.traineeFinal.repository;

import com.api.traineeFinal.model.PurchaseOrder;

import java.util.List;

public interface PurchaseOrderRepository {
    public List<PurchaseOrder> getPurchaseOrders();
    public PurchaseOrder createPurchaseOrder(PurchaseOrder purchaseOrder);
    public PurchaseOrder getPurchaseOrderById(int id);
}
