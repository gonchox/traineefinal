package com.api.traineeFinal.repository;

import com.api.traineeFinal.model.IngredientXDish;
import com.api.traineeFinal.model.OrderDetail;

import java.util.List;

public interface OrderDetailRepository {
    public List<OrderDetail> getOrderDetailByPurchaseOrderId(int purchaseOrderId);
    public List<OrderDetail> getOrderDetailByDishId(int dishId);
    public OrderDetail createOrderDetail(OrderDetail orderDetail);
    List<OrderDetail> getOrderDetailsWithDishesByPurchaseOrderId(int purchaseOrderId);
}
