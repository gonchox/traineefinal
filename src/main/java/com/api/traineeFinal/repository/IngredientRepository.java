package com.api.traineeFinal.repository;

import com.api.traineeFinal.model.DishType;
import com.api.traineeFinal.model.Ingredient;

import java.util.List;

public interface IngredientRepository {
    public List<Ingredient> getIngredients();
    public Ingredient createIngredient(Ingredient ingredient);
    public Ingredient getIngredientById(int id);
}
