package com.api.traineeFinal.repository;

import com.api.traineeFinal.model.IngredientXDish;

import java.util.List;

public interface IngredientXDishRepository {
    public List<IngredientXDish> getIngredientXDishByDishId(int dishId);
    public List<IngredientXDish> getIngredientXDishByIngredientId(int ingredientId);
    public IngredientXDish createIngredientXDish(IngredientXDish ingredientXDish);
    List<IngredientXDish> getIngredientXDishesWithIngredientsByDishId(int dishId);
}
