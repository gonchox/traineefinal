package com.api.traineeFinal.repository;

import com.api.traineeFinal.model.DishType;

import java.util.List;

public interface DishTypeRepository {
    public List<DishType> getDishTypes();
    public DishType createDishType(DishType dishType);
    public DishType getDishTypeById(int id);
}
