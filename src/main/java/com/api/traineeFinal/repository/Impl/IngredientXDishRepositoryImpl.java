package com.api.traineeFinal.repository.Impl;

import com.api.traineeFinal.model.Ingredient;
import com.api.traineeFinal.model.IngredientXDish;
import com.api.traineeFinal.repository.IngredientXDishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class IngredientXDishRepositoryImpl implements IngredientXDishRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<IngredientXDish> getIngredientXDishByDishId(int dishId) {
        String sql = "SELECT * FROM IngredientXDish WHERE idDish = ?";
        List<IngredientXDish> ingredientXDishes = jdbcTemplate.query(sql,
                (rs, rowNum) -> mapToIngredientXDish(rs), dishId);
        return ingredientXDishes;
    }

    @Override
    public List<IngredientXDish> getIngredientXDishByIngredientId(int ingredientId) {
        String sql = "SELECT * FROM IngredientXDish WHERE idIngredient = ?";
        List<IngredientXDish> ingredientXDishes = jdbcTemplate.query(sql,
                (rs, rowNum) -> mapToIngredientXDish(rs), ingredientId);
        return ingredientXDishes;
    }

    private IngredientXDish mapToIngredientXDish(ResultSet rs) throws SQLException {
        IngredientXDish ingredientXDish = new IngredientXDish();
        ingredientXDish.setId(rs.getInt("id"));
        Ingredient ingredient = new Ingredient();
        ingredient.setId(rs.getInt("idIngredient"));
        ingredientXDish.setQuantity(rs.getInt("quantity"));

       ingredientXDish.setIngredient(ingredient);

        return ingredientXDish;
    }

    @Override
    public IngredientXDish createIngredientXDish(IngredientXDish ingredientXDish) {
        SimpleJdbcInsert simpleJdbcInsert =
                new SimpleJdbcInsert(jdbcTemplate.getDataSource())
                        .withTableName("IngredientXDish")
                        .usingGeneratedKeyColumns("id");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idDish", ingredientXDish.getDish().getId());
        parameters.put("idIngredient", ingredientXDish.getIngredient().getId());
        parameters.put("quantity", ingredientXDish.getQuantity());
        int id = simpleJdbcInsert.executeAndReturnKey(parameters).intValue();
        ingredientXDish.setId(id);
        return ingredientXDish;


    }

    @Override
    public List<IngredientXDish> getIngredientXDishesWithIngredientsByDishId(int dishId) {
        String sql = "SELECT ixd.*, i.* FROM IngredientXDish ixd JOIN Ingredient i ON ixd.idIngredient = i.id WHERE ixd.idDish = ?";
        return jdbcTemplate.query(sql, (rs, rowNum) -> {
            IngredientXDish ingredientXDish = new IngredientXDish();
            ingredientXDish.setId(rs.getInt("id"));
            ingredientXDish.setQuantity(rs.getInt("quantity"));
            Ingredient ingredient = new Ingredient();
            ingredient.setId(rs.getInt("id"));
            ingredient.setName(rs.getString("name"));
            ingredient.setIngredientPrice(rs.getDouble("ingredientPrice"));
            ingredientXDish.setIngredient(ingredient);
            return ingredientXDish;
        }, dishId);
    }
}
