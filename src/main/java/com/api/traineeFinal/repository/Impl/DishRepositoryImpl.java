package com.api.traineeFinal.repository.Impl;

import com.api.traineeFinal.model.Dish;
import com.api.traineeFinal.model.DishType;
import com.api.traineeFinal.model.Ingredient;
import com.api.traineeFinal.repository.DishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DishRepositoryImpl implements DishRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Dish> getDishes() {
        String sql = "SELECT * FROM Dish";
        List<Dish> dishes = jdbcTemplate.query(sql,
                (rs, rowNum) -> mapToDish(rs));
        return dishes;
    }

    public Dish mapToDish(ResultSet rs) throws SQLException {
        Dish dish= new Dish();
        dish.setId(rs.getInt("id"));
        dish.setName(rs.getString("name"));
        dish.setDescription(rs.getString("description"));
        dish.setDishPrice(rs.getDouble("dishPrice"));
        DishType dishType = new DishType();
        dishType.setId(rs.getInt("idDishType"));
        dish.setDishType(dishType);

        return dish;
    }

    @Override
    public Dish createDish(Dish dish) {
        SimpleJdbcInsert simpleJdbcInsert =
                new SimpleJdbcInsert(jdbcTemplate.getDataSource())
                        .withTableName("Dish")
                        .usingGeneratedKeyColumns("id");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idDishType", dish.getDishType().getId());
        parameters.put("name",dish.getName());
        parameters.put("description",dish.getDescription());
        parameters.put("dishPrice",dish.getDishPrice());
        int id = simpleJdbcInsert.executeAndReturnKey(parameters).intValue();
        dish.setId(id);
        return dish;
    }

    @Override
    public Dish getDishById(int id) {
        String sql = "SELECT * FROM Dish WHERE id = ?";
        Dish dish = jdbcTemplate.queryForObject(sql,
                (rs, rowNum) -> mapToDish(rs), id);
        return dish;
    }
}
