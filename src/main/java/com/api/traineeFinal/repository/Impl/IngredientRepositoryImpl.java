package com.api.traineeFinal.repository.Impl;

import com.api.traineeFinal.model.Ingredient;
import com.api.traineeFinal.repository.IngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class IngredientRepositoryImpl implements IngredientRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Ingredient> getIngredients() {
        String sql = "SELECT * FROM Ingredient";
        List<Ingredient> ingredients = jdbcTemplate.query(sql,
                (rs, rowNum) -> mapToIngredient(rs));
        return ingredients;
    }

    public Ingredient mapToIngredient(ResultSet rs) throws SQLException {
        return new Ingredient(
                rs.getInt("id"),
                rs.getString("name"),
                rs.getDouble("ingredientPrice"));
    }

    @Override
    public Ingredient createIngredient(Ingredient ingredient) {
        SimpleJdbcInsert simpleJdbcInsert =
                new SimpleJdbcInsert(jdbcTemplate.getDataSource())
                        .withTableName("Ingredient")
                        .usingGeneratedKeyColumns("id");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", ingredient.getName());
        parameters.put("ingredientPrice",ingredient.getIngredientPrice());
        int id = simpleJdbcInsert.executeAndReturnKey(parameters).intValue();
        ingredient.setId(id);
        return ingredient;
    }

    @Override
    public Ingredient getIngredientById(int id) {
        String sql = "SELECT * FROM Ingredient WHERE id = ?";
        Ingredient ingredient = jdbcTemplate.queryForObject(sql,
                (rs, rowNum) -> mapToIngredient(rs), id);
        return ingredient;
    }
}
