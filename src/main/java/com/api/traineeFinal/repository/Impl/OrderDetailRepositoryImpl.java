package com.api.traineeFinal.repository.Impl;

import com.api.traineeFinal.model.Dish;
import com.api.traineeFinal.model.OrderDetail;
import com.api.traineeFinal.model.PurchaseOrder;
import com.api.traineeFinal.repository.OrderDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderDetailRepositoryImpl implements OrderDetailRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<OrderDetail> getOrderDetailByPurchaseOrderId(int purchaseOrderId) {
        String sql = "SELECT * FROM OrderDetail WHERE idPurchaseOrder = ?";
        List<OrderDetail> orderDetailList = jdbcTemplate.query(sql,
                (rs, rowNum) -> mapToOrderDetail(rs), purchaseOrderId);
        return orderDetailList;
    }

    @Override
    public List<OrderDetail> getOrderDetailByDishId(int dishId) {
        String sql = "SELECT * FROM OrderDetail WHERE idDish = ?";
        List<OrderDetail> orderDetailList = jdbcTemplate.query(sql,
                (rs, rowNum) -> mapToOrderDetail(rs), dishId);
        return orderDetailList;
    }

    private OrderDetail mapToOrderDetail(ResultSet rs) throws SQLException {
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setId(rs.getInt("id"));
        Dish dish = new Dish();
        dish.setId(rs.getInt("idDish"));

        orderDetail.setDish(dish);

        return orderDetail;
    }

    @Override
    public OrderDetail createOrderDetail(OrderDetail orderDetail) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate.getDataSource())
                .withTableName("OrderDetail")
                .usingGeneratedKeyColumns("id");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idDish", orderDetail.getDish().getId());
        parameters.put("idPurchaseOrder", orderDetail.getPurchaseOrder().getId());
        int id = simpleJdbcInsert.executeAndReturnKey(parameters).intValue();
        orderDetail.setId(id);

        return orderDetail;
    }

    @Override
    public List<OrderDetail> getOrderDetailsWithDishesByPurchaseOrderId(int purchaseOrderId) {
        String sql = "SELECT od.*, po.* FROM OrderDetail od JOIN PurchaseOrder po ON od.idPurchaseOrder = po.id WHERE od.idDish = ?";
        return jdbcTemplate.query(sql, (rs, rowNum) -> {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setId(rs.getInt("id"));
            PurchaseOrder purchaseOrder = new PurchaseOrder();
           purchaseOrder.setId(rs.getInt("id"));
            orderDetail.setPurchaseOrder(purchaseOrder);
            return orderDetail;
        }, purchaseOrderId);
    }
}
