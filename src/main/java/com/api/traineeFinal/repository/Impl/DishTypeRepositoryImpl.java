package com.api.traineeFinal.repository.Impl;

import com.api.traineeFinal.model.DishType;
import com.api.traineeFinal.repository.DishTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DishTypeRepositoryImpl implements DishTypeRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<DishType> getDishTypes() {
        String sql = "SELECT * FROM DishType";
        List<DishType> dishTypes = jdbcTemplate.query(sql,
                (rs, rowNum) -> mapToDishType(rs));
        return dishTypes;
    }

    public DishType mapToDishType(ResultSet rs) throws SQLException {
        return new DishType(
                rs.getInt("id"),
                rs.getString("name"));
    }

    @Override
    public DishType createDishType(DishType dishType) {
        SimpleJdbcInsert simpleJdbcInsert =
                new SimpleJdbcInsert(jdbcTemplate.getDataSource())
                        .withTableName("DishType")
                        .usingGeneratedKeyColumns("id");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", dishType.getName());
        int id = simpleJdbcInsert.executeAndReturnKey(parameters).intValue();
        dishType.setId(id);
        return dishType;
    }

    @Override
    public DishType getDishTypeById(int id) {
        String sql = "SELECT * FROM DishType WHERE id = ?";
        DishType dishType = jdbcTemplate.queryForObject(sql,
                (rs, rowNum) -> mapToDishType(rs), id);
        return dishType;
    }
}
