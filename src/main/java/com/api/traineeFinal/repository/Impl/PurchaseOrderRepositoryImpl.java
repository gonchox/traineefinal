package com.api.traineeFinal.repository.Impl;

import com.api.traineeFinal.model.Dish;
import com.api.traineeFinal.model.OrderDetail;
import com.api.traineeFinal.model.PurchaseOrder;
import com.api.traineeFinal.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class PurchaseOrderRepositoryImpl implements PurchaseOrderRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<PurchaseOrder> getPurchaseOrders() {
        String sql = "SELECT * FROM PurchaseOrder";
        List<PurchaseOrder> purchaseOrders = jdbcTemplate.query(sql,
                (rs, rowNum) -> mapToPurchaseOrder(rs));
        return purchaseOrders;
    }

    private PurchaseOrder mapToPurchaseOrder(ResultSet rs) throws SQLException {
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setId(rs.getInt("id"));
        Dish dish = new Dish();
        dish.setId(rs.getInt("idDish"));
        purchaseOrder.setTotalPrice(rs.getDouble("totalPrice"));

        return purchaseOrder;
    }

    @Override
    public PurchaseOrder createPurchaseOrder(PurchaseOrder purchaseOrder) {
        SimpleJdbcInsert simpleJdbcInsert =
                new SimpleJdbcInsert(jdbcTemplate.getDataSource())
                        .withTableName("PurchaseOrder")
                        .usingGeneratedKeyColumns("id");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("totalPrice", purchaseOrder.getTotalPrice());
        int id = simpleJdbcInsert.executeAndReturnKey(parameters).intValue();
        purchaseOrder.setId(id);

        for (OrderDetail orderDetail : purchaseOrder.getOrderDetails()) {
            simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate.getDataSource())
                    .withTableName("OrderDetail")
                    .usingGeneratedKeyColumns("id");

            parameters = new HashMap<>();
            parameters.put("idPurchaseOrder", purchaseOrder.getId());
            parameters.put("idDish", orderDetail.getDish().getId());
            simpleJdbcInsert.execute(parameters);
        }

        return purchaseOrder;
    }



    @Override
    public PurchaseOrder getPurchaseOrderById(int id) {
        String sql = "SELECT * FROM PurchaseOrder WHERE id = ?";
        PurchaseOrder purchaseOrder = jdbcTemplate.queryForObject(sql,
                (rs, rowNum) -> mapToPurchaseOrder(rs), id);
        return purchaseOrder;
    }
}
