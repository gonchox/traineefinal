package com.api.traineeFinal.repository;

import com.api.traineeFinal.model.Dish;
import com.api.traineeFinal.model.DishType;

import java.util.List;

public interface DishRepository {
    public List<Dish> getDishes();
    public Dish createDish(Dish dish);
    public Dish getDishById(int id);
}
