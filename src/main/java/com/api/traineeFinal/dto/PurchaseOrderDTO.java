package com.api.traineeFinal.dto;

import com.api.traineeFinal.model.OrderDetail;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PurchaseOrderDTO {
    private List<OrderDetailDTO> orderDetails;
}
