package com.api.traineeFinal.dto;

import lombok.Data;

import java.util.ArrayList;

@Data
public class DishDTO {
    private String name;
    private String description;
    private int idDishType;
    private ArrayList<IngredientXDishDTO> ingredientXDishDTOS;
}
