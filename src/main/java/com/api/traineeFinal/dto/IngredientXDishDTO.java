package com.api.traineeFinal.dto;

import lombok.Data;

@Data
public class IngredientXDishDTO {
    private int idIngredient;
    private int quantity;
}
