package com.api.traineeFinal.controller;

import com.api.traineeFinal.dto.DishTypeDTO;
import com.api.traineeFinal.dto.IngredientDTO;
import com.api.traineeFinal.model.DishType;
import com.api.traineeFinal.model.Ingredient;
import com.api.traineeFinal.service.DishTypeService;
import com.api.traineeFinal.service.IngredientService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(IngredientController.class)
public class IngredientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IngredientService ingredientService;

    @Test
    void getIngredientsTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        Ingredient ingredient = new Ingredient(1, "ensalada",7.99);
        Ingredient ingredient1 = new Ingredient(2, "tomate",7.89);
        List<Ingredient> expectedIngredients = new ArrayList<>();
        expectedIngredients.add(ingredient);
        expectedIngredients.add(ingredient1);
        int expectedStatus = 200;

        String uri = "/v1/ingredients";

        when(ingredientService.getIngredients()).thenReturn(expectedIngredients);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        List<Ingredient> actualIngredients = mapper.readValue(content,
                new TypeReference<List<Ingredient>>(){});

        assertEquals(expectedStatus, actualStatus);
        assertEquals(expectedIngredients.size(), actualIngredients.size());
        assertEquals(expectedIngredients, actualIngredients);
    }

    @Test
    void getIngredientsErrorTest() throws Exception{

        int expectedStatus = 500;

        String uri = "/v1/ingredients";

        when(ingredientService.getIngredients()).thenThrow(new NullPointerException("Error"));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);
    }

    @Test
    void createIngredientTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        IngredientDTO ingredientDTO = new IngredientDTO();
        ingredientDTO.setName("lechuga");
        ingredientDTO.setIngredientPrice(7.89);

        Ingredient expectedIngredient = new Ingredient(1, "lechuga",7.89);
        int expectedStatus = 201;

        String uri = "/v1/ingredients";

        String bodyString = mapper.writeValueAsString(ingredientDTO);

        when(ingredientService.createIngredient(any(IngredientDTO.class))).thenReturn(expectedIngredient);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Ingredient actualIngredient = mapper.readValue(content, Ingredient.class);

        assertEquals(expectedStatus, actualStatus);
        assertEquals(actualIngredient.getName(), expectedIngredient.getName());
    }

    @Test
    void createIngredientErrorTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        IngredientDTO ingredientDTO = new IngredientDTO();
        ingredientDTO.setName("lechuga");
        ingredientDTO.setIngredientPrice(7.99);

        int expectedStatus = 500;

        String uri = "/v1/ingredients";

        String bodyString = mapper.writeValueAsString(ingredientDTO);

        when(ingredientService.createIngredient(any(IngredientDTO.class))).thenThrow(new NullPointerException("Error"));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);
    }
}
