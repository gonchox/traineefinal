package com.api.traineeFinal.controller;

import com.api.traineeFinal.dto.DishTypeDTO;
import com.api.traineeFinal.model.DishType;
import com.api.traineeFinal.service.DishTypeService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(DishTypeController.class)
public class DishTypeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DishTypeService dishTypeService;

    @Test
    void getDishTypesTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        DishType dishType = new DishType(1, "ensalada");
        DishType dishType1 = new DishType(1, "sopa");
        List<DishType> expectedDishTypes = new ArrayList<>();
        expectedDishTypes.add(dishType);
        expectedDishTypes.add(dishType1);
        int expectedStatus = 200;

        String uri = "/v1/dishTypes";

        when(dishTypeService.getDishTypes()).thenReturn(expectedDishTypes);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        List<DishType> actualDishTypes = mapper.readValue(content,
                new TypeReference<List<DishType>>(){});

        assertEquals(expectedStatus, actualStatus);
        assertEquals(expectedDishTypes.size(), actualDishTypes.size());
        assertEquals(expectedDishTypes, actualDishTypes);
    }

    @Test
    void getDishTypesErrorTest() throws Exception{

        int expectedStatus = 500;

        String uri = "/v1/dishTypes";

        when(dishTypeService.getDishTypes()).thenThrow(new NullPointerException("Error"));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);
    }

    @Test
    void createDishTypeTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        DishTypeDTO dishTypeDTO = new DishTypeDTO();
        dishTypeDTO.setName("ensalada");

        DishType expectedDishType = new DishType(1, "ensalada");
        int expectedStatus = 201;

        String uri = "/v1/dishTypes";

        String bodyString = mapper.writeValueAsString(dishTypeDTO);

        when(dishTypeService.createDishType(any(DishTypeDTO.class))).thenReturn(expectedDishType);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        DishType actualDishType = mapper.readValue(content, DishType.class);

        assertEquals(expectedStatus, actualStatus);
        assertEquals(actualDishType.getName(), expectedDishType.getName());
    }

    @Test
    void createDishTypeErrorTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        DishTypeDTO dishTypeDTO = new DishTypeDTO();
        dishTypeDTO.setName("ensalada");

        int expectedStatus = 500;

        String uri = "/v1/dishTypes";

        String bodyString = mapper.writeValueAsString(dishTypeDTO);

        when(dishTypeService.createDishType(any(DishTypeDTO.class))).thenThrow(new NullPointerException("Error"));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);
    }
}
