package com.api.traineeFinal.repository;

import com.api.traineeFinal.model.Dish;
import com.api.traineeFinal.model.DishType;
import com.api.traineeFinal.repository.Impl.DishRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DishRepositoryTest {

    @Mock
    private JdbcTemplate jdbcTemplate;

    @InjectMocks
    private DishRepositoryImpl dishRepository;

    @Test
    void getDishesTest() {
        DishType dishType1 = new DishType(1,"sopa");
        DishType dishType2 = new DishType(2,"ensalada");

        Dish dish = new Dish(1,dishType1, "sopa de leche","sopita",70.99);
        Dish dish1 = new Dish(2,dishType2, "ensalada rusa","ensaladita",39.99);
        List<Dish> expectedDishes = new ArrayList<>();
        expectedDishes.add(dish);
        expectedDishes.add(dish1);

        when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(expectedDishes);

        List<Dish> actualDishes =dishRepository.getDishes();
        assertEquals(expectedDishes.size(),actualDishes.size());
        assertEquals(expectedDishes, actualDishes);

    }


    @Test
    void getDishByIdTest() {
        //Expected
        int id = 1;
        DishType dishType1 = new DishType(1,"sopa");
        Dish expectedDish = new Dish(1,dishType1, "sopa de leche","sopita",70.99);

        when(jdbcTemplate.queryForObject(eq("SELECT * FROM Dish WHERE id = ?"),
                any(RowMapper.class), eq(id))).thenReturn(expectedDish);

        Dish actualDish = dishRepository.getDishById(id);

        assertNotNull(actualDish);
        assertEquals(actualDish.getName(), expectedDish.getName());
    }



    @Test
    void mapToDishTest() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        //Expected
        DishType dishType1 = new DishType(1,"sopa");
        Dish expectedDish = new Dish(1,dishType1, "sopa de leche","sopita",70.99);

        when(resultSet.getInt("id")).thenReturn(1);
        when(resultSet.getString("name")).thenReturn("sopa de leche");

        Dish actualDish = dishRepository.mapToDish(resultSet);

        assertNotNull(actualDish);
        assertEquals(actualDish.getId(), expectedDish.getId());
        assertEquals(actualDish.getName(), expectedDish.getName());

    }
}
