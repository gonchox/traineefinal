package com.api.traineeFinal.repository;

import com.api.traineeFinal.model.DishType;
import com.api.traineeFinal.repository.Impl.DishTypeRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DishTypeRepositoryTest {
    @Mock
    private JdbcTemplate jdbcTemplate;

    @InjectMocks
    private DishTypeRepositoryImpl dishTypeRepository;

    @Test
    void getDishTypesTest() {
        //Expected
        DishType dishType = new DishType(1, "ensalada");
        DishType dishType1 = new DishType(2, "sopa");
        List<DishType> expectedDishTypes = new ArrayList<>();
        expectedDishTypes.add(dishType);
        expectedDishTypes.add(dishType1);

        when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(expectedDishTypes);

        List<DishType> actualDishTypes =dishTypeRepository.getDishTypes();
        assertEquals(expectedDishTypes.size(),actualDishTypes.size());
        assertEquals(expectedDishTypes, actualDishTypes);

    }


    @Test
    void getDishTypeByIdTest() {
        //Expected
        int id = 1;
        DishType expectedDishType = new DishType(1, "ensalada");

        when(jdbcTemplate.queryForObject(eq("SELECT * FROM DishType WHERE id = ?"),
                any(RowMapper.class), eq(id))).thenReturn(expectedDishType);

        DishType actualDishType = dishTypeRepository.getDishTypeById(id);

        assertNotNull(actualDishType);
        assertEquals(actualDishType.getName(), expectedDishType.getName());
    }



    @Test
    void mapToDishTypeTest() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        //Expected
        DishType expectedDishType = new DishType(1, "ensalada");

        when(resultSet.getInt("id")).thenReturn(1);
        when(resultSet.getString("name")).thenReturn("ensalada");

        DishType actualDishType = dishTypeRepository.mapToDishType(resultSet);

        assertNotNull(actualDishType);
        assertEquals(actualDishType.getId(), expectedDishType.getId());
        assertEquals(actualDishType.getName(), expectedDishType.getName());

    }
}
