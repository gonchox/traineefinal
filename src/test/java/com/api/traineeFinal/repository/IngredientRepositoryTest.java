package com.api.traineeFinal.repository;

import com.api.traineeFinal.model.DishType;
import com.api.traineeFinal.model.Ingredient;
import com.api.traineeFinal.repository.Impl.DishTypeRepositoryImpl;
import com.api.traineeFinal.repository.Impl.IngredientRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class IngredientRepositoryTest {

    @Mock
    private JdbcTemplate jdbcTemplate;

    @InjectMocks
    private IngredientRepositoryImpl ingredientRepository;

    @Test
    void getIngredientsTest() {
        //Expected
        Ingredient ingredient = new Ingredient(1, "lechuga",7.99);
        Ingredient ingredient2 = new Ingredient(2, "tomate",7.99);
        List<Ingredient> expectedIngredients = new ArrayList<>();
        expectedIngredients.add(ingredient);
        expectedIngredients.add(ingredient2);

        when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(expectedIngredients);

        List<Ingredient> actualIngredients =ingredientRepository.getIngredients();
        assertEquals(expectedIngredients.size(),actualIngredients.size());
        assertEquals(expectedIngredients, actualIngredients);

    }


    @Test
    void getDishTypeByIdTest() {
        //Expected
        int id = 1;
        Ingredient expectedIngredient = new Ingredient(1, "lechuga",7.99);

        when(jdbcTemplate.queryForObject(eq("SELECT * FROM Ingredient WHERE id = ?"),
                any(RowMapper.class), eq(id))).thenReturn(expectedIngredient);

        Ingredient actualIngredient = ingredientRepository.getIngredientById(id);

        assertNotNull(actualIngredient);
        assertEquals(actualIngredient.getName(), expectedIngredient.getName());
    }



    @Test
    void mapToDishTypeTest() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        //Expected
        Ingredient expectedIngredient = new Ingredient(1, "lechuga",7.99);

        when(resultSet.getInt("id")).thenReturn(1);
        when(resultSet.getString("name")).thenReturn("lechuga");
        when(resultSet.getDouble("ingredientPrice")).thenReturn(7.99);

        Ingredient actualIngredient = ingredientRepository.mapToIngredient(resultSet);

        assertNotNull(actualIngredient);
        assertEquals(actualIngredient.getId(), expectedIngredient.getId());
        assertEquals(actualIngredient.getName(), expectedIngredient.getName());

    }
}
