package com.api.traineeFinal.service;

import com.api.traineeFinal.dto.DishDTO;
import com.api.traineeFinal.model.Dish;
import com.api.traineeFinal.model.DishType;
import com.api.traineeFinal.repository.DishRepository;
import com.api.traineeFinal.repository.DishTypeRepository;
import com.api.traineeFinal.service.Impl.DishServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DishServiceTest {

    @Mock
    private DishRepository dishRepository;
    @Mock
    private DishTypeRepository dishTypeRepository;

    @InjectMocks
    private DishServiceImpl dishService;

    @Test
    void getDishesTest() {
        DishType dishType1 = new DishType(1,"sopa");
        DishType dishType2 = new DishType(2,"ensalada");

        Dish dish = new Dish(1,dishType1, "sopa de leche","sopita",70.99);
        Dish dish1 = new Dish(2,dishType2, "ensalada rusa","ensaladita",39.99);
        List<Dish> expectedDishes = new ArrayList<>();
        expectedDishes.add(dish);
        expectedDishes.add(dish1);

        when(dishRepository.getDishes()).thenReturn(expectedDishes);

        List<Dish> actualDishes = dishRepository.getDishes();
        assertEquals(expectedDishes.size(), actualDishes.size());
        assertEquals(expectedDishes, actualDishes);
    }

    @Test
    void getDishByIdTest() {
        int id = 1;
        DishType dishType1 = new DishType(1,"sopa");
        Dish expectedDish = new Dish(1,dishType1, "sopa de leche","sopita",70.99);

        when(dishRepository.getDishById(id)).thenReturn(expectedDish);

        Dish actualDish = dishService.getDishById(id);

        assertNotNull(actualDish);
        assertEquals(actualDish.getName(), actualDish.getName());
    }

    @Test
    void createDishTest() {
        DishType dishType = new DishType();
        dishType.setId(1);
        dishType.setName("sopa");

        DishDTO dishDTO = new DishDTO();
        dishDTO.setName("sopa de leche");
        dishDTO.setDescription("sopita");
        dishDTO.setIdDishType(1);

        when(dishTypeRepository.getDishTypeById(1)).thenReturn(dishType);

        Dish expectedDish = new Dish();
        expectedDish.setName("sopa de leche");
        expectedDish.setDescription("sopita");
        expectedDish.setDishType(dishType);

        when(dishRepository.createDish(any(Dish.class))).thenReturn(expectedDish);

        Dish actualDish = dishService.createDish(dishDTO);

        assertNotNull(actualDish);
        assertEquals(expectedDish.getName(), actualDish.getName());
        assertEquals(expectedDish.getDishType(), actualDish.getDishType());
    }
}
