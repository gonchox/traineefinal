package com.api.traineeFinal.service;

import com.api.traineeFinal.dto.DishTypeDTO;
import com.api.traineeFinal.dto.IngredientDTO;
import com.api.traineeFinal.model.DishType;
import com.api.traineeFinal.model.Ingredient;
import com.api.traineeFinal.repository.DishTypeRepository;
import com.api.traineeFinal.repository.IngredientRepository;
import com.api.traineeFinal.service.Impl.DishTypeServiceImpl;
import com.api.traineeFinal.service.Impl.IngredientServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class IngredientServiceTest {

    @Mock
    private IngredientRepository ingredientRepository;

    @InjectMocks
    private IngredientServiceImpl ingredientService;

    @Test
    void getIngredientsTest() {
        Ingredient ingredient = new Ingredient(1, "lechuga",7.99);
        Ingredient ingredient2 = new Ingredient(2, "tomate",7.99);
        List<Ingredient> expectedIngredients = new ArrayList<>();
        expectedIngredients.add(ingredient);
        expectedIngredients.add(ingredient2);

        when(ingredientRepository.getIngredients()).thenReturn(expectedIngredients);

        List<Ingredient> actualIngredients = ingredientService.getIngredients();
        assertEquals(expectedIngredients.size(), actualIngredients.size());
        assertEquals(expectedIngredients, actualIngredients);
    }

    @Test
    void getIngredientByIdTest() {
        int id = 1;
        Ingredient expectedIngredient = new Ingredient(1, "lechuga",7.99);

        when(ingredientRepository.getIngredientById(id)).thenReturn(expectedIngredient);

        Ingredient actualIngredient = ingredientService.getIngredientById(id);

        assertNotNull(actualIngredient);
        assertEquals(actualIngredient.getName(), expectedIngredient.getName());
    }

    @Test
    void createIngredientTest() {
        IngredientDTO ingredientDTO = new IngredientDTO();
        ingredientDTO.setName("lechuga");
        ingredientDTO.setIngredientPrice(7.99);

        Ingredient expectedIngredient = new Ingredient();
        expectedIngredient.setName("lechuga");
        expectedIngredient.setIngredientPrice(7.99);

        when(ingredientRepository.createIngredient(any(Ingredient.class))).thenReturn(expectedIngredient);

        Ingredient actualIngredient = ingredientService.createIngredient(ingredientDTO);

        assertNotNull(actualIngredient);
        assertEquals(actualIngredient.getName(), expectedIngredient.getName());
    }
}
