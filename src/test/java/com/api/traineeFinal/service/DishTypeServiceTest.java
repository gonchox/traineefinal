package com.api.traineeFinal.service;

import com.api.traineeFinal.dto.DishTypeDTO;
import com.api.traineeFinal.model.DishType;
import com.api.traineeFinal.repository.DishTypeRepository;
import com.api.traineeFinal.service.Impl.DishTypeServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DishTypeServiceTest {
    @Mock
    private DishTypeRepository dishTypeRepository;

    @InjectMocks
    private DishTypeServiceImpl dishTypeService;

    @Test
    void getDishTypesTest() {
        DishType dishType = new DishType(1, "ensalada");
        DishType dishType1 = new DishType(2, "sopa");
        List<DishType> expectedDishTypes = new ArrayList<>();
        expectedDishTypes.add(dishType);
        expectedDishTypes.add(dishType1);

        when(dishTypeRepository.getDishTypes()).thenReturn(expectedDishTypes);

        List<DishType> actualDishTypes = dishTypeService.getDishTypes();
        assertEquals(expectedDishTypes.size(), actualDishTypes.size());
        assertEquals(expectedDishTypes, actualDishTypes);
    }

    @Test
    void getWarehouseTypeByIdTest() {
        int id = 1;
        DishType expectedDishType = new DishType(1, "ensalada");

        when(dishTypeRepository.getDishTypeById(id)).thenReturn(expectedDishType);

        DishType actualDishType = dishTypeService.getDishTypeById(id);

        assertNotNull(actualDishType);
        assertEquals(actualDishType.getName(), expectedDishType.getName());
    }

    @Test
    void createDishTypeTest() {
        DishTypeDTO dishTypeDTO = new DishTypeDTO();
        dishTypeDTO.setName("ensalada");

        DishType expectedDishType = new DishType();
        expectedDishType.setName("ensalada");

        when(dishTypeRepository.createDishType(any(DishType.class))).thenReturn(expectedDishType);

        DishType actualDishType = dishTypeService.createDishType(dishTypeDTO);

        assertNotNull(actualDishType);
        assertEquals(actualDishType.getName(), expectedDishType.getName());
    }
}
